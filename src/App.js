import React from 'react';
import logo from './logo.svg';
import './App.css';
import SeccionExperiencia from './Components/SeccionExperiencia/SeccionExperiencia';
import SeccionNosotros from './Components/SeccionNosotros/SeccionNosotros';

function App() {
  return (
    <div>
      <SeccionExperiencia></SeccionExperiencia>
      <SeccionNosotros></SeccionNosotros>
    </div>
  );
}

export default App;
