import React, { Component } from 'react';

import './SeccionExperiencia.css'

import { 
    Container 
} from 'react-bootstrap';

class SeccionExperiencia extends Component {
    render() {
        return (
            <div>
                <img className="img-fluid" src="https://i.ibb.co/1XnKLct/Ondas-banner.png" />
                <Container align="center" className="container_experiencia">
                    <h4 style={{color: '#9D927E'}}>Experiencia</h4>
                    <h2>Mas cerca <strong>de la vida</strong></h2>
                    <p>
                     Lorem ipsum dolor sit amet, consectetuer adipiscing elit, 
                     sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna 
                     aliquam erat volutpat. 
                     Lorem ipsum dolor sit amet consectetur adipiscing elit congue conubia habitant, 
                     senectus torquent dictumst viverra est quam a nibh dui, nisl vivamus lectus libero non placerat 
                     enim id fringilla. Curabitur imperdiet habitant duis eros.
                    </p>
                </Container>
                <img className="img-fluid" src="https://i.ibb.co/1XnKLct/Ondas-banner.png" />
            </div>
        );
    }
}

export default SeccionExperiencia;