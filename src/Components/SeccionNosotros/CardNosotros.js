import React, { Component } from 'react';
import { 
    Card,
    Button
} from 'react-bootstrap';
class CardNosotros extends Component {
    constructor(props) {
        super(props)

    }

    render() {
        const card          = this.props.card
        const showImage     = this.props.showImage
        const leaveImage    = this.props.leaveImage
        const cardChange    = this.props.cardChange

        return (
            <Card
                className={'card_nosotros '+cardChange}
                onMouseEnter={showImage }
                onMouseLeave={leaveImage}
            >
                <img src={card.icon} className="" alt="..."></img>
                <Card.Body>{card.text}</Card.Body>
                <Button style={{width:'30%'}} variant="secondary">Más</Button>
            </Card>
        );
    }
}

export default CardNosotros;