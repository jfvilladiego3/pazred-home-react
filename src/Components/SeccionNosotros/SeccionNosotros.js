import React, { Component } from 'react';

import './SeccionNosotros.css'

import { 
    Container,
    Row,
    Col
} from 'react-bootstrap';

import CardNosotros from './CardNosotros'

class SeccionNosotros extends Component {

    constructor(props){
        super(props)
        this.state = {
            changeClass: '',
            cardChange: '',

            cards: [
                { icon: 'https://i.ibb.co/tZ58kzW/catedra.png', text: 'Cátedra de Paz', url: 'btn' },
                { icon: 'https://i.ibb.co/94dNRTm/Instituciones.png', text: 'Proyecto PazRed', url: 'btn'},
                { icon: 'https://i.ibb.co/Qbnvyq6/ideas.png', text: 'Instituciones Aliadas', url: 'btn' },
                { icon: 'https://i.ibb.co/chLYmVR/equipo.png', text: 'Nuestro Equipo', url: 'btn' },
              ],
            
        }
        this.showImage = this.showImage.bind(this)
        this.leaveImage = this.leaveImage.bind(this)
    }

    showImage(){
        this.setState({
              changeClass: 'mountainsChange',
              cardChange: 'cardChange',
  
          })
      }
  
      leaveImage(){
          this.setState({
              changeClass: '',
              cardChange: '',
          })
      }
  renderCardsNosotros(){
    return (
        this.state.cards.map((card, index) => (
                <Col xs={12} sm={12} md={3} key={index} >
                    <CardNosotros
                    cardChange={this.state.cardChange}
                    showImage={this.showImage}
                    leaveImage={this.leaveImage}
                    
                    card={card} 
                    />
                </Col>
            )
        )
    )
}

  

    render() {
        return (
            <div>
                <div className="container_Nosotros">
                    <div className="img_container_nosotros cielo"></div>
                    <div className={'img_container_nosotros montains '+ this.state.changeClass}></div> 
                    <div className="img_container_nosotros niebla"></div>
                    
                </div>
                <div className="container_section_nosotros">
                <Container align="center">
                    <div className="container_section_title_nosotros">
                        <h3>Acerca de Nosotros</h3>
                        <span> 
                            elit, sed diam nonummy nibh euismod tincidunt ut
                        </span>
                    </div>
                    <Row className="container_cards">
                        {this.renderCardsNosotros()}
                    </Row>
                </Container>
            </div>
        </div>
        );
    }
}

export default SeccionNosotros;